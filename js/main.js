(function(){

    var fn = {
        httpGet: function( callback ){
            var http = new XMLHttpRequest();
            http.onreadystatechange = function(){
                if( this.status == 200 && this.readyState == 4 ){
                    callback( this.responseText );
                }
            }
            http.open("GET", '/products.json', false);
            http.send();
        },
        mobMenu: function(){
            document.querySelector("header#header div.mob-menu").onclick = function(){
                
                var el = document.querySelector('header#header .top-header-wrapper');
                var overlay = document.querySelector('header#header div.overlay-mob-menu');
                
                if( el.classList.contains('ativo') ){
                    el.classList.remove("ativo");
                    overlay.style.display = 'none';
                }else{
                    el.classList.add("ativo");
                    overlay.style.display = 'block';
                }

                overlay.onclick = function(){
                    el.classList.remove("ativo");
                    this.style.display = 'none';
                }
            }

            document.querySelector('header#header div.header-wrapper>div.main-menu nav ul li:first-of-type').onclick = function(){
                if( this.classList.contains('open') ){
                    this.classList.remove('open');
                    var display = 'none';
                }else{
                    this.classList.add('open');
                    var display = 'block';
                }
                var temp = document.querySelectorAll('header#header div.header-wrapper>div.main-menu nav ul li');
                for( var item in temp ){
                    if( item > 0 ){
                        temp[item].style.display = display;
                    }
                }
            }
        },
        selectImg: function(){
            var thumbs = document.querySelectorAll("section#main div.thumbs ul li");
            for( var item in thumbs ){
                thumbs[item].onclick = function(){
                    var getImg = this.querySelector('img').getAttribute('src');
                    document.querySelector('section#main div.main-image img').setAttribute('src', getImg);
                }
            }
        },
        callProduct: function(){
            this.httpGet(function( response ){
                var parse = JSON.parse( response );
                var product = parse[0];

                document.querySelector(".product-infos .product-name h1").innerText = product.name;
                document.querySelector(".product-infos .product-id").innerText = product.id;
                document.querySelector(".product-infos .product-description p").innerText = product.description;

                for( var color in product.colors ){
                    var list = document.querySelector(".product-infos .product-colors ul");
                    var listItem = document.createElement('LI');
                    listItem.style.backgroundColor = product.colors[color];
                    list.appendChild( listItem );
                }

                document.querySelector(".product-infos .product-price span.price").innerText = product.prices.price;
                document.querySelector(".product-infos .product-price span.per").innerText = product.prices.promotion;
                document.querySelector(".product-infos .product-price div.installment strong").innerText = product.prices.installment;

                document.querySelector('.images-wrapper>div.main-image img').setAttribute('src', product.images[0]);

                for( var image in product.images ){
                    var thumbsList = document.querySelector("div.thumbs ul");
                    var thumb = document.createElement('LI');
                    thumb.innerHTML = "<img src='" + product.images[image] + "' alt='" + product.name + "'>";
                    thumbsList.appendChild( thumb );
                }
            });
        },
        init: function(){
            this.mobMenu();
            this.callProduct();
            this.selectImg();
        }
    }

    fn.init();

})();