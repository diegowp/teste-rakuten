### Teste prático Rakuten
> Candidato: Diego Teixeira
### Tecnologias utilizadas
 - HTML,
 - CSS / SASS / FlexBox
 - JS ( vanilla )
 - NodeJS ( Gulp, BrowserSync )
 - Rest API ( exemplo bem simples )
### Instruções
Executar no terminal:
> $ npm install
> $ gulp
### Observações em geral
Não tive tempo para desenvolver algo mais complexo para  carrossel de produtos, a ideia inicial seria desenvolver algo simples com JS puro.
Uma solução rápida porém dependendo de bibliotecas de terceiros, seria utilizar o Owl Carousel junto com jQuery.